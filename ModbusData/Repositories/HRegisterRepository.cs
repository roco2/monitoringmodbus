using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ModbusCore.Exceptions;
using ModbusCore.Entities;
using ModbusData.Interfaces;

namespace ModbusData.Repositories
{
    public class HRegisterRepository : IHRegisterRepository
    {
        private readonly MbContext _mbContext;

        public HRegisterRepository()
        {
            _mbContext = new MbContext();
        }
        //public async Task<HRegister> Create(HRegister hregister)
        public  HRegister Create(HRegister hregister)
        {
            if (hregister == null)
            {
                throw new ExceptionHRegister
                ("Entity can't be null..!!!");
            }

            try
            {
                _mbContext.HRegisters.Add(hregister);
                _mbContext.SaveChangesAsync();
            
            }
            catch (System.Exception e)
            {
                var m = e.Message;
            }
            return hregister;
        }

        public IEnumerable<HRegister> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<HRegister> GetbyAddress(string address)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<HRegister> GetbyDate(DateTime date1, DateTime date2)
        {
            throw new NotImplementedException();
        }
    }
}