using System;
using System.Collections.Generic;
using ModbusCore.Entities;
using System.Threading.Tasks;

namespace ModbusData.Interfaces
{
    public interface IHRegisterRepository
    {
         IEnumerable<HRegister> GetAll();
         IEnumerable<HRegister> GetbyAddress(string address);
         IEnumerable<HRegister> GetbyDate(DateTime date1, DateTime date2);
         //Task<HRegister> Create(HRegister hregister);
         HRegister Create(HRegister hregister);
         

         
    }
}