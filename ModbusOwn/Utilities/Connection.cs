using Modbus.Device;
using ModbusCore;
using System.Net.Sockets;

namespace ModbusOwn.Utilities
{
    public class Connection
    {
        public bool status = false;
        private TcpClient tcpClient;
        private ModbusIpMaster master;
        public void Start(){
            try
            {
                string ipAddr = FileConfig.Ip;
                int tcpPort = FileConfig.Port;

                tcpClient = new TcpClient();
                tcpClient.BeginConnect(ipAddr, tcpPort, null, null);
                master = ModbusIpMaster.CreateIp(tcpClient);
                status = true;
            }
            catch (System.Exception e)
            {
                var c = e.Message;
                status = false;
            }
        }

        public ushort[] Read(int positionStart, int size)
        {
            ushort[] data=null;
            
            if(status)
                data = master.ReadHoldingRegisters(2, (ushort)positionStart, (ushort)size);

            return data;
        }
    }
}