using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ModbusCore;

namespace ModbusOwn.Utilities
{
    public class Inspector
    {
        public ushort[] data;
        public static string[] response;
        public bool process;
        Connection connection = new Connection();
        Thread hilo;
        public void Start(){
            process = true;
            hilo = new Thread(Main);
            hilo.Start();
        }

        public string[] Getdata()
        {             
            //Task.Delay(500).Wait();
            return response;
        }

        private void Main(){
            while(process){
                    try{
                            if(!connection.status)
                            {
                                connection.Start();
                            }
                            else{
                                var init = FileConfig.StartAddress;
                                response = new string[FileConfig.AmmountVariable];

                                for (ushort i = 0; i < FileConfig.AmmountVariable; i++)
                                {
                                    data =  connection.Read(init,2);
                                    //var f = ConvertFloat(data[1],data[0],FileConfig.Presition);
                                    var f = ConvertInt(data);
                                    response[i] = f.ToString();
                                    init =  init + 2;
                                }
                            }
                    }
                    catch(Exception ex){
                        connection.status = false;
                        Console.WriteLine("Error of type " +  ex.Message);
                        //Console.ReadLine();
                        if(!connection.status)
                        {   
                            Console.WriteLine("Reconected.....");
                            connection.Start();
                            Task.Delay(500).Wait(); 
                            Console.Clear();
                        }
                    }
            }
        }

        public float ConvertFloat(ushort msb, ushort lsb, int presition)
        {
            var ret = BitConverter.GetBytes(msb).Concat(BitConverter.GetBytes(lsb)).ToArray();
            float res = (float) Math.Round(BitConverter.ToSingle(ret,0),presition);
            return res;
        }

        public int ConvertInt(ushort[] value)
        {
            var ret = value[1];
            return int.Parse(ret.ToString());
        }

    }
}
