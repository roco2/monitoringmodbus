using System;

namespace ModbusCore.Entities
{
    public class HRegister
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string Value { get; set; }
        public DateTime DateCreated { get; set; }

    }
}