using System;
using System.Globalization;

namespace ModbusCore.Exceptions
{
    public class ExceptionHRegister: Exception
    {
        public ExceptionHRegister() : base() {}

        public ExceptionHRegister(string exceptionMessage) : base(exceptionMessage) { }

        public ExceptionHRegister(string exceptionMessage, params object[] args) 
            : base(String.Format(CultureInfo.CurrentCulture, exceptionMessage, args))
        {
        }
    }
}