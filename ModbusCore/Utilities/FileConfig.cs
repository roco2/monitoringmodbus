using System.IO;
using Microsoft.Extensions.Configuration;

namespace ModbusCore
{
    public static class FileConfig
    {
        private static IConfigurationBuilder builder = 
                new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("Parameters.json", optional: true, reloadOnChange: true);           
        public static string Ip = builder.Build().GetSection("Ip").Value;
        public static int Port = int.Parse(builder.Build().GetSection("Port").Value);
        public static int StartAddress = int.Parse(builder.Build().GetSection("FirstAddress").Value);
        public static int AmmountVariable = int.Parse(builder.Build().GetSection("AmmountVariable").Value);
        public static int Presition = int.Parse(builder.Build().GetSection("Presition").Value);
        public static string Connection = builder.Build().GetSection("ConnectionString").Value;
        
    }
}